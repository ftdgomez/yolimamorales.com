<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/fslightbox/index.js"></script>
<script type="text/javascript">
      if (window.innerWidth > 580) {
        var tpj = jQuery;

        var revapi1068;
        tpj(document).ready(function () {
          if (tpj("#main_slider").revolution == undefined) {
            revslider_showDoubleJqueryError("#main_slider");
          } else {
            revapi1068 = tpj("#main_slider")
              .show()
              .revolution({
                sliderType: "standart",
                jsFileLocation: "<?php echo get_stylesheet_directory_uri() ?>/revolution/js/",
                sliderLayout: "fullscreen",
                dottedOverlay: "none",
                delay: 2000,
                navigation: {
                  keyboardNavigation: "on",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation: "off",
                  mouseScrollReverse: "default",
                  onHoverStop: "off",
                  touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false,
                  },
                  bullets: {
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 720,
                    style: "uranus",
                    hide_onleave: true,
                    direction: "horizontal",
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 15,
                    space: 5,
                    tmp: '<span class="tp-bullet-inner"></span>',
                  },
                  arrows: {
                    enable: true,
                    style: "hesperiden",
                    tmp: "",
                    rtl: false,
                    hide_onleave: false,
                    hide_onmobile: true,
                    hide_under: 0,
                    hide_over: 9999,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    left: {
                      container: "slider",
                      h_align: "left",
                      v_align: "center",
                      h_offset: 20,
                      v_offset: 0,
                    },

                    right: {
                      container: "slider",
                      h_align: "right",
                      v_align: "center",
                      h_offset: 20,
                      v_offset: 0,
                    },
                  },
                },
                viewPort: {
                  enable: true,
                  outof: "wait",
                  visible_area: "80%",
                  presize: false,
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1024, 778, 480],
                gridheight: [868, 768, 960, 720],
                lazyType: "single",
                shadow: 0,
                spinner: "true",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: 1,
                shuffle: "off",
                autoHeight: "true",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: ".header",
                fullScreenOffset: "",
                disableProgressBar: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                  simplifyAll: "off",
                  nextSlideOnWindowFocus: "off",
                  disableFocusListener: false,
                },
              });
          }
        }); /*ready*/
      }
    </script>
    <script>
      if (window.matchMedia("(min-width: 700px)").matches) {
        window.onscroll = function () {
          let logo = document.querySelector(".logo");
          if (
            document.body.scrollTop > 50 ||
            document.documentElement.scrollTop > 50
          ) {
            document.getElementById("header").classList.add("header-scroll");
            logo.style.height = "50px";
          } else {
            document.getElementById("header").classList.remove("header-scroll");
            logo.style.height = "100px";
          }
        };
      }
      window.onresize = () => {
        window.location.reload();
      };
    </script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>

<footer class="p-2 text-center border-t border-light bg-white">
    <div class="mb-1 ">
      <?php
        $rows = get_field('otros_enlaces', 'option');
        if ($rows): ?>
      <?php foreach( $rows as $row): ?>
        <a href="<?php echo $row['link'] ?>">
            <img
            style="
                display: block;
                max-width: 140px;
                margin: auto;
                margin-bottom: .5em
            "
            src="<?php echo $row['imagen'] ?>"
            alt=""
            />
        </a>
      <?php endforeach; ?>
      <?php endif; ?>
      </div>
        <p>
            <?php echo get_field('texto_footer', 'option') ?>
        </p>
</footer>
<script>
    let btn = document.querySelector('.burger-menu');
    let navbar = document.querySelector('.navbar-m');
    let menuBtns = document.querySelectorAll('.navbar a');
    btn.addEventListener('click', ()=>{
        navbar.classList.toggle('navbar-m-active')
    })
    menuBtns.forEach(el => {
        el.addEventListener('click', ()=>{
        navbar.classList.toggle('navbar-m-active')
    })
    })
</script>
