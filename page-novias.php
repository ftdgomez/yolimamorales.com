<?php get_header() ?>

<section class="container-xl mt-4 pt-2" id="galeria">
      <div class="super-grid mb-1">
          <?php 
          $grid_imgs = get_field('novias_fotos', 'option');
          if ($grid_imgs):
          ?>

        <img
          data-aos="fade-right"
          data-aos-delay="400"
          class="g-1"
          src="<?php echo $grid_imgs[0] ?>"
          alt=""
          loading="lazy"
        />
        <div
          data-aos="fade-down"
          data-aos-delay="400"
          class="g-2 bg-img"
          style="
            align-self: end;
            background-image: url(<?php echo $grid_imgs[1] ?>);
            height: 100%;
            width: 100%;
          "
        ></div>
        <div
          data-aos="fade-up"
          data-aos-delay="400"
          class="g-3 bg-img"
          style="
            align-self: end;
            background-image: url(<?php echo $grid_imgs[2] ?>);
            height: 100%;
            width: 100%;
          "
        ></div>

        <div data-aos="fade-left" data-aos-delay="400" class="g-x">
          <h4 class="thin text-lg">Ver Galería</h4>
          <a data-fslightbox href="<?php echo $grid_imgs[0] ?>" class="btn bg-dark text-center border">Ver Galería</a>
        </div>
        <img
          data-aos="fade-left"
          data-aos-delay="400"
          class="g-4"
          src="<?php echo $grid_imgs[3] ?>"
          alt=""
          loading="lazy"
        />
    </div>
    <?php // Loop para rellenar la galeria ?>
        <?php foreach($grid_imgs as $key=>$img): ?>
          <?php if($key > 1): ?>
            <a data-fslightbox href="<?php echo $img ?>"></a>
          <?php endif; ?>
        <?php endforeach ?>
          <?php endif; ?>
    </section>



<?php 

    get_template_part( 'template-parts/content', 'contact' );
get_footer() ?>
