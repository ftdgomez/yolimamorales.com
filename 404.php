<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package yolimamorales
 */
get_header();
?>
<div class="container flex align-center justify-center h-screen">
<p class="text-center">
    Oh! parece que te has perdido. Quieres <a class="color-primary" href="/">Volver al inicio?</a>
</p>
</div>
<?php
get_footer();
