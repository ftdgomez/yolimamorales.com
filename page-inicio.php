<?php
get_header();
/**
 * Import rev slider
 */
get_template_part( 'template-parts/content', 'revslider' );
?>
    <div data-aos="fade-up" class="container mt-4">
      <h4 class="light text-xl text-center">
        <?php the_field('primera_frase', 'option') ?>
      </h4>
    </div>
<?php 
/**
 *  @section    Galeria 
 */
?>
    <section class="container-xl" id="galeria">
      <div class="super-grid">
          <?php 
          $grid_imgs = get_field('galeria_de_fotos', 'option');
          if ($grid_imgs):
          ?>

        <img
          data-aos="fade-right"
          data-aos-delay="400"
          class="g-1"
          src="<?php echo $grid_imgs[0] ?>"
          alt=""
          loading="lazy"
        />
        <div
          data-aos="fade-down"
          data-aos-delay="400"
          class="g-2 bg-img"
          style="
            align-self: end;
            background-image: url(<?php echo $grid_imgs[1] ?>);
            height: 100%;
            width: 100%;
          "
        ></div>
        <div
          data-aos="fade-up"
          data-aos-delay="400"
          class="g-3 bg-img"
          style="
            align-self: end;
            background-image: url(<?php echo $grid_imgs[2] ?>);
            height: 100%;
            width: 100%;
          "
        ></div>

        <div data-aos="fade-left" data-aos-delay="400" class="g-x">
          <h4 class="thin text-lg">Ver Galería</h4>
          <a data-fslightbox href="<?php echo $grid_imgs[4] ?>" class="btn bg-dark text-center border">Ver Galería</a>
        </div>
        <img
          data-aos="fade-left"
          data-aos-delay="400"
          class="g-4"
          src="<?php echo $grid_imgs[3] ?>"
          alt=""
          loading="lazy"
        />
        <?php // Loop para rellenar la galeria ?>
        <?php foreach($grid_imgs as $key=>$img): ?>
          <?php if($key > 3): ?>
            <a data-fslightbox href="<?php echo $img ?>"></a>
          <?php endif; ?>
        <?php endforeach ?>
          <?php endif; ?>
      </div>
    </section>
    <?php 
    /**
     *  @section    About Me 
     */
    ?>
    <section id="acerca-de-mi" class="container-xl custom-grid-kk gap-2 py-4">
      <div
        class="border-t border-light p-2 bg-white"
        style="grid-column: 1/5; align-self: center;"
      >
      <p>
          <b class="text-lg d-block">Soy Yolima Morales</b>
      </p>
        <?php the_field('about_me_i', 'option') ?>
          </div>
      <div style="grid-column: 5/9;">
        <img src="<?php echo the_field('foto_centro_about_me', 'option') ?>" loading="lazy" alt="" />
      </div>
        <div
        style="grid-column: 9/14; align-self: center;"
        class="border-t border-light p-2 bg-white"
        >
            <?php the_field('about_me_d', 'option') ?>
        </div>
    </section>
    <?php
    /**
     *  @section    Frase previa a Contacto
     */
    ?>
    <section class="container-xl">
      <div class="custom-grid-kk">
        <div style="grid-column: 1/8; grid-row: 1;" class="flex align-center">
          <img src="<?php echo the_field('foto_previa_contacto', 'option') ?>" alt="" loading="lazy" />
        </div>
        <div
          style="grid-column: 6/12; grid-row: 1;"
          class="flex align-center justify-center"
        >
          <div class="bg-white p-3 shadow">
            <?php echo the_field('texto_previo_contacto', 'option') ?>
          </div>
        </div>
      </div>
    </section>
<?php
    /**
     *  @section    Map -& Contact
     */
    get_template_part( 'template-parts/content', 'contact' );
    get_footer();
?>
