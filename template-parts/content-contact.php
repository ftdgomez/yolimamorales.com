 <section id="contacto" class="container-xl py-4 grid-2 gap-1 bg-white">
      <div class="pt-4">
        <div class="form-container border border-light" action="">
          <h4 class="text-md bold mb-1"><?php echo the_field('titulo_contacto', 'option') ?></h4>
            <p class="mb-1">
            <?php echo the_field('texto_contacto', 'option') ?>
            </p>
            <?php echo do_shortcode('[contact-form-7 id="32" title="Formulario de contacto"]'); ?>
        </div>
      </div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2215.468034815886!2d-3.5919649502425792!3d40.366645990161196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4224d0104bd045%3A0x40222b33626cb38b!2sCalle%20Fernando%20Chueca%20Goitia%2C%2016A%2C%2028051%20Madrid!5e0!3m2!1ses!2ses!4v1590530246360!5m2!1ses!2ses"
        width="100%"
        height="699px"
        frameborder="0"
        style="border: 0;"
        allowfullscreen=""
        aria-hidden="false"
        tabindex="0"
        class="mt-4"
      ></iframe>
</section>
