
    <div
      id="main_slider_wrapper"
      class="rev_slider_wrapper fullscreen-container"
      style="background-color: transparent; padding: 0px;"
    >
      <!-- START SLIDER -->
      <div
        id="main_slider"
        class="rev_slider fullscreenbanner"
        style="display: none;"
        data-version="5.4.1"
      >
      <?php 
      $images = get_field('carousel_principal', 'option');
      if($images): ?>
        <ul>
            <?php foreach( $images as $key=>$image_url): ?>
            <!-- SLIDE  -->
            <li data-index="rs-0<?php echo $key + 1 ?>" data-transition="fade">
              <img
              src="<?php echo $image_url ?>"
              alt=""
              data-lazyload="<?php echo $image_url ?>"
              data-kenburns="on"
              data-bgposition="center right"
              data-duration="20000"
              data-ease="Power1.easeOut"
              data-scalestart="110"
              data-scaleend="100"
              data-rotatestart="0"
              data-rotateend="0"
              data-offsetstart="0 0"
              data-offsetend="0 0"
              class="rev-slidebg"
              data-no-retina
            />
          </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
       </div>
      <!-- END SLIDER -->
    </div>
    <div
      id="mobile-hero"
      class="bg-img h-screen"
      style="background-image: url(<?php echo $images[0] ?>);"
      loading="lazy"
    ></div>
